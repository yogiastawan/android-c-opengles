package com.ether.oglestest

class LibJNIWrapper {

    external fun onSurfaceCreated()
    external fun onSurfaceChanged(width: Int, height: Int)
    external fun onDrawFrame()

    external fun setEyeX(floatv: Float)
    external fun setEyeY(floatv: Float)
    external fun setEyeZ(floatv: Float)

    companion object {
        // Used to load the 'oglestest' library on application startup.
        init {
            System.loadLibrary("oglestest")
        }
    }
}