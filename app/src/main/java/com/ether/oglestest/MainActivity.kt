package com.ether.oglestest

import android.app.ActivityManager
import android.content.Context
import android.opengl.GLSurfaceView
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.slider.Slider

class MainActivity : AppCompatActivity() {

    private var isRender: Boolean = false
    private lateinit var glSurf: GLSurfaceView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val activityManager: ActivityManager =
            getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val configurationService = activityManager.deviceConfigurationInfo
        isRender = configurationService.glEsVersion >= 0x2000.toString() || isEmu()
        val renderWrapper = RenderWrapper()
        if (isRender) {
            glSurf = findViewById(R.id.gl_surface)
            if (isEmu()) {
                glSurf.setEGLConfigChooser(8, 8, 8, 8, 16, 0)
            }
            glSurf.setEGLContextClientVersion(2)
            glSurf.setRenderer(renderWrapper)
            glSurf.visibility = View.VISIBLE
        } else {
            Toast.makeText(applicationContext, "OpenGlES Not found", Toast.LENGTH_LONG).show()
        }

        val progX: Slider = findViewById(R.id.eye_x)
        progX.addOnChangeListener { _, value, _ ->
            renderWrapper.setEyeX(value)
        }
        val progY: Slider = findViewById(R.id.eye_y)
        progY.addOnChangeListener { _, value, _ ->
            renderWrapper.setEyeY(value)
        }
        val progZ: Slider = findViewById(R.id.eye_z)
        progZ.addOnChangeListener { _, value, _ ->
            renderWrapper.setEyeZ(value)
        }

    }

    override fun onPause() {
        super.onPause()
        if (isRender) {
            glSurf.onPause()
        }
    }

    override fun onResume() {
        super.onResume()
        if (isRender) {
            glSurf.onResume()
        }
    }

    /**
     * A native method that is implemented by the 'oglestest' native library,
     * which is packaged with this application.
     */


    private fun isEmu(): Boolean {
        return (Build.FINGERPRINT.startsWith(
            "generic"
        ) || Build.FINGERPRINT.startsWith("unknown") || Build.MODEL.contains("google_sdk") || Build.MODEL.contains(
            "Emulator"
        ) || Build.MODEL.contains("Android SDK built for x86"))
    }
}