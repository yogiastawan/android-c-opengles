package com.ether.oglestest

import android.opengl.GLSurfaceView
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class RenderWrapper : GLSurfaceView.Renderer {
    override fun onSurfaceCreated(p0: GL10?, p1: EGLConfig?) {
        LibJNIWrapper().onSurfaceCreated()
    }

    override fun onSurfaceChanged(p0: GL10?, p1: Int, p2: Int) {
        LibJNIWrapper().onSurfaceChanged(p1, p2)
    }

    override fun onDrawFrame(p0: GL10?) {
        LibJNIWrapper().onDrawFrame()
    }

    fun setEyeX(value: Float) {
        LibJNIWrapper().setEyeX(value)
    }

    fun setEyeY(value: Float) {
        LibJNIWrapper().setEyeY(value)
    }

    fun setEyeZ(value: Float) {
        LibJNIWrapper().setEyeZ(value)
    }
}