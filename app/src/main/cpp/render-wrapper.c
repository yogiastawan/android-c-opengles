//
// Created by yogiastawan on 3/11/22.
//

#include "render-wrapper.h"
#include "gl-wrapper.h"
#include "oglparselib/file_output/cube_01.h"
#include "oglparselib/file_output/cube2_cube_001.h"
#include "android/log.h"
#include "utils.h"

#include "linmath/linmath.h"
#include <jni.h>

#define LOG_TAG "OGLES TEST"

static inline float deg_to_radf(float deg);

static void position_object_in_scene(float x, float y, float z);

static mat4x4 projection_matrix;
static mat4x4 model_matrix;
static mat4x4 view_matrix;


static mat4x4 view_projection_matrix;
static mat4x4 model_view_projection_matrix;
static mat4x4 inverted_view_projection_matrix;

static GLint POSITION_DATA_SIZE = 3;
static GLint COLOR_DATA_SIZE = 4;
static GLint NORMAL_DATA_SIZE = 3;

static GLuint program;
static GLuint program2;

static GLint normal_handler;
static GLuint position_handler;

float eye_x = 0.f, eye_y = 0.f, eye_z = 0.f;

static const char *vertexShaderSource = "attribute vec4 a_Position;\n"
                                        "uniform mat4 uMVPMatrix;\n"
                                        "\n"
                                        "void main()\n"
                                        "{\n"
                                        "    gl_Position = uMVPMatrix*a_Position;\n"
                                        "}";
static const char *fragmentShaderSource = "precision mediump float;\n"
                                          "\n"
                                          "\n"
                                          "void main()\n"
                                          "{\n"
                                          "    gl_FragColor = vec4(0,0,1,1);//v_color;\n"
                                          "}";

void on_surface_created() {
    glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    program = create_program(vertexShaderSource, fragmentShaderSource);
    program2 = create_program(vertexShaderSource, fragmentShaderSource);
    if (!program) {
        __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "Cannot Create Program");
        return;
    }
}

void on_surface_changed(int width, int height) {
    glViewport(0, 0, width, height);
    mat4x4_perspective(projection_matrix, deg_to_radf(45), (float) width / (float) height, 1.0f,
                       10.0f);
//    mat4x4_look_at(view_matrix, (vec3) {0.0f, 0.0f, 10.f}, (vec3) {0.0f, 0.0f, 0.0f},
//                   (vec3) {0.0f, 1.0f, 0.0f});
}

void on_draw_frame() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program);
    mat4x4_look_at(view_matrix, (vec3) {eye_x, eye_y, eye_z}, (vec3) {0.0f, 0.0f, 0.0f},
                   (vec3) {0.0f, 1.0f, 0.0f});
    mat4x4_mul(view_projection_matrix, projection_matrix, view_matrix);
    position_object_in_scene(0.f, 0.f, 0.f);
    GLint mVPMLocation = glGetUniformLocation(program, "uMVPMatrix");
    glUniformMatrix4fv(mVPMLocation, 1, GL_FALSE, (const GLfloat *) model_view_projection_matrix);
    position_handler = (GLuint) glGetAttribLocation(program, "a_Position");
    glVertexAttribPointer(position_handler, POSITION_DATA_SIZE, GL_FLOAT, GL_FALSE, 0,
                          cube_01_positions);
    glEnableVertexAttribArray(position_handler);
    glDrawArrays(GL_TRIANGLES, 0, cube_01_vertice);
    glDisableVertexAttribArray(position_handler);

    //2
    glUseProgram(program2);
    position_object_in_scene(0.f, 0.f, 0.f);
    GLint mVPMLocation2 = glGetUniformLocation(program2, "uMVPMatrix");
    glUniformMatrix4fv(mVPMLocation2, 1, GL_FALSE, (const GLfloat *) model_view_projection_matrix);
    position_handler = (GLuint) glGetAttribLocation(program2, "a_Position");
    glVertexAttribPointer(position_handler, POSITION_DATA_SIZE, GL_FLOAT, GL_FALSE, 0,
                          cube2_cube_001_positions);
    glEnableVertexAttribArray(position_handler);
    glDrawArrays(GL_TRIANGLES, 0, cube2_cube_001_vertice);
    glDisableVertexAttribArray(position_handler);

}

static inline float deg_to_radf(float deg) {
    return deg * (float) M_PI / 180.0f;
}

static void position_object_in_scene(float x, float y, float z) {
    mat4x4_identity(model_matrix);
//    mat4x4_translate_in_place(model_matrix, x, y, z);
    mat4x4_mul(model_view_projection_matrix, view_projection_matrix, model_matrix);
}

JNIEXPORT void JNICALL
Java_com_ether_oglestest_LibJNIWrapper_setEyeX(JNIEnv *env, jobject thiz, jfloat floatf) {
    eye_x = floatf;
}

JNIEXPORT void JNICALL
Java_com_ether_oglestest_LibJNIWrapper_setEyeY(JNIEnv *env, jobject thiz, jfloat floatf) {
    eye_y = floatf;
}

JNIEXPORT void JNICALL
Java_com_ether_oglestest_LibJNIWrapper_setEyeZ(JNIEnv *env, jobject thiz, jfloat floatf) {
    eye_z = floatf;
}