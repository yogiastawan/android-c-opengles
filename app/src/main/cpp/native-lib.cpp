#include <jni.h>
//#include <string>
#include "render-wrapper.h"

extern "C"
JNIEXPORT void JNICALL
Java_com_ether_oglestest_LibJNIWrapper_onSurfaceCreated(JNIEnv *env, jobject thiz) {
    on_surface_created();
}

extern "C"
JNIEXPORT void JNICALL
Java_com_ether_oglestest_LibJNIWrapper_onDrawFrame(JNIEnv *env, jobject thiz) {
    on_draw_frame();
}
extern "C"
JNIEXPORT void JNICALL
Java_com_ether_oglestest_LibJNIWrapper_onSurfaceChanged(JNIEnv *env, jobject thiz, jint width,
                                                        jint height) {
    on_surface_changed((int)width, (int)height);
}
