//
// Created by yogiastawan on 3/11/22.
//

#ifndef OGLES_TEST_RENDER_WRAPPER_H
#define OGLES_TEST_RENDER_WRAPPER_H

#ifdef __cplusplus
extern "C" {
#endif

void on_surface_created();

void on_surface_changed(int width, int height);

void on_draw_frame();

#ifdef __cplusplus
}
#endif

#endif //OGLES_TEST_RENDER_WRAPPER_H
