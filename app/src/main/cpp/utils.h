//
// Created by yogiastawan on 3/13/22.
//

#ifndef OGLES_TEST_UTILS_H
#define OGLES_TEST_UTILS_H

#include "gl-wrapper.h"

#ifdef __cplusplus
extern "C" {
#endif

    GLuint load_shader(GLenum type, const char  *source);
    GLuint create_program(const char *vertexShaderSource, const char *fragmentShaderSource);

#ifdef __cplusplus
}
#endif

#endif //OGLES_TEST_UTILS_H
