//
// Created by yogiastawan on 3/13/22.
//

#include "utils.h"


GLuint load_shader(GLenum type, const char *source) {
    GLuint shader = glCreateShader(type);
    if (!shader) {
        return 0;
    }
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);
    return shader;
}

GLuint create_program(const char *vertexShaderSource, const char *fragmentShaderSource) {
    GLuint program = glCreateProgram();
    if (!program) {
        return 0;
    }
    GLuint vertexShader = load_shader(GL_VERTEX_SHADER, vertexShaderSource);
    if (!vertexShader) { return 0; }
    GLuint fragmentShader = load_shader(GL_FRAGMENT_SHADER, fragmentShaderSource);
    if (!fragmentShader) { return 0; }
    glAttachShader(program,vertexShader);
    glAttachShader(program,fragmentShader);
    glLinkProgram(program);
    return program;
}